import React, {useEffect} from 'react';
import {FlatList, Text, Platform} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ProductItem from '../../components/shop/ProductItem';
import * as cartActions from '../../store/actions/cart';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';

const ProductOverviewScreen = props => {
  const [data, setData] = React.useState();

  useEffect(() => {
    fetch(
      'https://apidojo-hm-hennes-mauritz-v1.p.rapidapi.com/products/list?country=asia2&lang=en&currentpage=0&pagesize=30&categories=men_all&concepts=H%26M%20MAN',
      {
        method: 'GET',
        headers: {
          'x-rapidapi-key':
            '63952442e3msh7a0f60c54ca3cc4p1aaaf3jsn8482fc527ff7',
          'x-rapidapi-host': 'apidojo-hm-hennes-mauritz-v1.p.rapidapi.com',
        },
      },
    )
      // .then(response => {
      //   console.warn('res', response.facets);
      //   setData(response);
      //   console.warn('data', setData);
      // })
      .then(response => response.json())
      .then(responseData => {
        console.warn('out of the', responseData.results[0].price);
        let d = responseData.results;
        setData(responseData.results);
        //  console.warn('out of ', d);
      })
      .catch(err => {
        console.error(err);
      });
  }, []);
  //const products = useSelector(state => state.products.availableProducts);
  const products = data;
  const dispatch = useDispatch();
  return (
    <FlatList
      data={products}
      renderItem={itemData => (
        <ProductItem
          image={itemData.item.images[0].url}
          title={itemData.item.name}
          price={itemData.item.price.value}
          onViewDetail={() => {
            props.navigation.navigate('ProductDetail', {
              productId: itemData.item,
              productTitle: itemData.item.title,
            });
          }}
          onAddToCart={() => {
            alert('item added to the cart successfully');
            dispatch(cartActions.addToCart(itemData.item));
          }}
        />
      )}
    />
  );
};

ProductOverviewScreen.navigationOptions = navData => {
  return {
    headerTitle: 'All Products',
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Cart"
          iconName="md-cart"
          onPress={() => {
            navData.navigation.navigate('Cart');
          }}
        />
      </HeaderButtons>
    ),
  };
};
export default ProductOverviewScreen;
